NOTE
====
The implementation of the class diagram is in the Entities directory

MY HOME WORK
============

Structs in C#
=============

The struct in C# programming languge is a value type that is usually used to emcapsulate small groups of related variables.

The structs can contain:
------------------------
- constructos
- constantes
- fields
- methods
- properties
- indexers
- events

Struct limitation in C#
-----------------------

Within a structure declaration, fields can not be initialized if they are't constant or static.

A struct can't declare a default constructor.

Struct properties in C#
-----------------------
- Structs are value type.
- Unlike classes, you can create instances of the structs without using new.
- Structs can declare constructors, but they must use paramaters.
- Structs can not inherit from other structs or class, they can't be base class. All structs inherit directly from System.ValueTuype
- A struct can implement interfaces.
 
Classes in C#
-------------

A class is a construction that allows you to create your own custom types by grouping variables of other types, methods, events. A class is a template that represents an abstraction of real life objects or a particular context
 
A class in C# is a construction of referential type

Within a class, attributes may or not be initiated, a class can be has a default constructor.

Class properties in C#
------------------------------

- Classes are a reference type
- In the class instantiation the word new is always used
- Classes can bi has overload methods
- Classes can inherit from other classes and can also implement interfaces

Structs vs Classes
---------------------

Can be said that the structs should be used if there are few fields and there are no classes that need to inherit from a class that has a definition similar to a struct.

The relevant difference is that the classes are of the referential type and the struct is the value type.

Multiple inheritance in C#
==========================

Multiple inheritance is not allowed in C#, however some OOP languages allow it, in the multiple inheritance type two or more super classes are used to derive a class. That is, the derived class shares the attributes and methods of more than one class

C# doesn't allow multiple inheritance, which means that classes can not inherit more than one classes. However, you can use interfaces for this purpose