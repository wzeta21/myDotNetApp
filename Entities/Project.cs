using System;

namespace dotnetApp.Entities {
    public class Project {
        public int Id {get; set;}
        public string Name {get;set;}
        public DateTime StartedDate {get;set;}
        public DateTime FinishDate {get; set;}

        public int ManagerId {get;set;}
        public Project(int managerId) {
            this.ManagerId = managerId;
        }
        
    }
}