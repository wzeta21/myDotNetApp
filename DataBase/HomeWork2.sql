--1. Registrar 7 personas con datos completos 
insert into Persona(persona_id, nombre, apellido, genero, fechaNacimiento, pais)
values (1, 'Jose', 'Lopez Illanes', 'f', '2018-01-20', 'Argentina'),
(2, 'Luis', 'Morales', 'm', '2000-02-25', 'Peru'),
(3, 'Dordival', 'Santos Moreira', 'm', '1995-05-28', 'Brasil'),
(4, 'Adenicia', 'De jesus Matos', 'f', '1990-06-15', 'Brasil'),
(5, 'Monalisa', 'Matos Neves', 'f', '1988-06-29', 'Brasil'),
(6, 'Jose', 'Zapata Ortiz', 'm', '2005-08-12', 'Bolivia'),
(7, 'Luana', 'Rocha Silva', 'm', '2010-12-20', 'Ecuador')

-- Registrar 3 personas sin nacionalidad
insert into Persona(persona_id, nombre, apellido, genero, fechaNacimiento)
values (8, 'Jose Luis', 'Sara Molle', 'm', '2008-01-20'),
(9, 'Elmer', 'Heromos Lopez', 'm', '1994-02-25'),
(10, 'Marcinho', 'Do santos', 'm', '1960-04-28'),
(44, 'Roberto Luis', 'Matos Neves', 'm', '1991-06-05')
-- Registrar 1 persona sin nacionalidad ni fecha de nacimiento
insert into Persona(persona_id, nombre, apellido, genero)
values (11, 'Jose Maria', 'Leyes Nogales', 'm')
-- Registrar 10 libros
insert into Libro(libro_id, titulo, precio, persona_id)
values
(102, 'Marianela', 50, 6),
(101, 'La mil y uno noche', 25, 5),
(103, 'Don quijote de la mancha', 100, 7),
(104, 'Object Oriented Programming in Java', 500, 2),
(105, 'PHP and Anjular', 145, 6),
(106, 'C# in 10 days', 104, 7),
(107, 'Lo que callas los perros', 5, 2),
(108, 'Ir a la luna', 15, 8),
(109, 'Python is Will and nice', 56, 10),
(110, 'Linux fundamentals', 104, 8)

-- Listar el nombre y apellido de la tabla “Persona” ordenados por apellido y luego por nombre.
select p.nombre, p.apellido
FROM Persona p
order by p.apellido, p. nombre

-- Listar los libros con precio mayor a 50 Bs. y que el titulo del libro empieze con la letra “A”
SELECT *
FROM libro l
where l.precio > 50 and l.titulo like 'A%'

-- Listar el nombre y apellido de las personas que tengan al menos un libro cuyo precio sea mayor a 50 Bs.
select p.nombre, p.apellido
from persona p INNER JOIN Libro l on p.persona_id = l.persona_id
where l.precio > 50
-- Listar el nombre y apellido de las personas que nacieron el año 1990
select p.nombre, p.apellido
from persona p
where p.fechaNacimiento like '1990%'
-- Listar los libros donde el titulo del libro contenga la letra “e” en la segunda posición
select l.titulo
from libro l
WHERE l.titulo like '_[e|é]%'
-- Contar el numero de registros de la tabla “Persona”
select count(p.persona_id) 'nro registros'
from persona p
-- Listar el o los libros más baratos
select top(3) *
from libro l
order by l.precio asc
-- Listar el o los libros más caros     
select top(3) *
from libro l
order by l.precio desc
-- Mostrar el promedio de precios de los libros
select avg(precio) 'precio promedio'
from Libro
-- Mostrar la sumatoria de los precios de los libros
select sum(precio) 'sumatorio de precio'
from Libro
-- Modificar la longitud del campo titulo para que soporte hasta titulos de 300 caracteres
alter table Libro alter column titulo varchar(300)
-- Listar las personas que sean del país de Colombia ó de Mexico
select *
from persona p
where p.pais in('colombia', 'Mexico')
-- Listar los libros que no empiezen con la letra “T”
select *
from libro l
where l.titulo not like 'T%'
-- Listar los libros que tengan un precio entre 50 a 70 Bs.
select *
from libro p
where p.precio BETWEEN 50 and 70
-- Listar las personas AGRUPADOS por país
select *
from Persona 
order by pais
-- Listar las personas AGRUPADOS por país y genero
select *
from Persona 
order by pais, genero
-- Listar las personas AGRUPADOS por genero, cuya cantidad sea mayor a 5
SELECT genero
FROM persona
GROUP BY genero 
having count(pais)>=5
-- Listar los libros ordenados por titulo en orden descendente
select *
from Libro l
order by l.titulo desc
-- Eliminar el libro con el titulo “SQL Server”
delete from libro
where titulo = 'SQL Server'
-- Eliminar los libros que comiencen con la letra “A”
delete from libro
where titulo like 'A%'
-- Eliminar las personas que no tengan libros
delete from persona
from persona
where persona_id not in (select persona_id from libro)
-- Eliminar todos los libros
delete from persona
from persona
where persona_id not in (select persona_id from libro)
-- Modificar el nombre de la persona llamada “Marco” por “Marcos”
update persona
set nombre = 'Marcos'
where nombre = 'Marco'
-- Modificar el precio del todos los libros que empiezen con la palabra “Calculo”  a  57 Bs.
update libro
set precio = 75
where titulo like 'Calculo%'
-- Inventarse una consulta con GROUP BY  y HAVING  y explicar el resultado que retorna.
select p.pais,  count(p.persona_id)
from Persona p
group by p.pais
having count(pais)=3
    --explicacion: esta consulta muestra el pais que se repite 3 vences entre los
    -- la nacionalidad de las personas


