using System;

namespace dotnetApp.Entities {
    public class Employee {
       public int Id {get;set;}
       public string Name {get;set;}
       public string SureNames {get;set;}
        public DateTime RecruitmentDate {get; set;}
    }
}