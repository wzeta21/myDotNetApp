--INER JOIN: El resultado de hacer un inner join es todo los datos comunes entre las dotas tablas involucradas,  es solo devuelve las tupals que tienen relación entre las dos tuplas involucradas en el join.
    --ejemplo:todas las personas que compraron libros 
select p.nombre, p.apellido, l.titulo, l.precio
from Persona p inner join Libro l on p.persona_id = l.persona_id
--SEMI JOIN: semi join es paresido a un natural join pero con la partiularidad de que existe where, esto lo que se vuelva semi-join
    --ejemplo1: devuelve todas bolivianso que compraron libros.
select p.nombre, p.apellido, l.titulo, l.precio
from Persona p inner join Libro l on p.persona_id = l.persona_id
where p.pais = 'Bolivia'

--LEFT OUTER JOIN: Este tipo de join devuelve todo lo que tenga la tabla de la izquierda mas lo que tengan en comun entre las dos tablas
--ejemplo:mostrar todoas las personas que mas sus libros
select p.nombre, p.apellido, l.titulo, l.precio
from Persona p left outer join Libro l on p.persona_id = l.persona_id

--RIGH OUTER JOIN: Este tipo de join devuelve todo lo que tenga la tabla de la derecha mas lo que tengan en comun entre las dos tablas
--ejemplo: Devolver todas los libros mas sus dueños
select p.nombre, p.apellido, l.titulo, l.precio
from Persona p right join Libro l on p.persona_id = l.persona_id

--FULL OUTER JOIN: Este tipo de join devuelve la union de las dos tablas, es decir devuelve todo lo que tenga realcion en las dos tabals y los que no tengan realción
--ejemplo: devolver la union de las tablas persona y libro
select *
from persona p full join Libro l on p.persona_id = l.persona_id

--CROSS JOIN:Este tipo de join presenta el producto cartesiano de los registros de las dos tablas. La tabla resultante tendrá todos los registros de la tabla izquierda combinados con cada uno de los registros de la tabla derecha. El código SQL para realizar este producto cartesiano enuncia las tablas que serán combinadas, pero no incluye algún predicado que filtre el resultado.
--ejemplo:
select *
from persona p cross join Libro l

--SELF JOIN: este tipo de join es cundo una tabla se saca una copia de si mismo
--ejemplo: duplica cada tupla donde su campo pais no es nulo
select *
from Persona p inner join Persona p2 on p.pais = p2.pais

--LEFT JOIN excluyendo la intersección: este tipo de join devuelve solo las tuplas de la tabla de la izquierda pero que tenga relacion con la otra tabla
--ejemplo:mustra todas las personas que no tienen libros comprados
select p.persona_id, p.nombre, p.apellido, l.titulo, l.precio
from Persona p left outer join Libro l on p.persona_id = l.persona_id
where l.titulo is null

--RIGHT JOIN excluyendo la intersección: este tipo de join devuelve solo las tuplas de la tabla derecha que tengan realcion con la tabla de la izquierda
--ejemplo:mostrar todas la personas que not tienen pais pero que que si compraron libros
select *
from Persona p right outer join Libro l on p.persona_id = l.persona_id
where p.pais is null

--FULL JOIN excluyendo la intersección: Este tipo de join devuelve todas las tuplas de las dos tablas exepto loq ue tiene en comun
--ejemplo: muestra todos los compardores que compraron sin saber el precio
select *
from persona p full join Libro l on p.persona_id = l.persona_id
where p.pais is null or l.libro_id is null


