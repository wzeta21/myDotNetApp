
using System.Collections.Generic;

namespace dotnetApp.Entities {
    public class Manager: Employee {
        private List<Employee> workers;
        private Project project;

        private bool enabled;
        /// <summary>
        /// cosntructor to warranty compsition
        /// </summary>
        public Manager() {
            this.project = new Project(this.Id);
            this.enabled = false;
        }
        /// <summary>
        /// constructor to warranty aggregation
        /// </summary>
        /// <param name="workers"></param>
        public Manager(List<Employee> workers) {
            this.workers = workers;
            this.project = new Project(this.Id);
            this.enabled = false;
        }

        /// <summary>
        /// getter to warranty composition
        /// </summary>
        /// <value></value>
        public List<Employee> Employees {
            get {
                return this.workers;
            }
        }
        /// <summary>
        /// setter to warranty aggregation
        /// </summary>
        /// <value></value>
        public Project Project{
            set {
                this.project = value;
            }
        }
        /// <summary>
        /// This function represents association
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public bool IsManager(Card card){
            this.enabled = card.GetPermissions(this.Id);
            return enabled;
        }
    }
}