-- 1. 
create database avanticaTrainingDB;
--2. 
drop database avanticaTrainingDB;
--3. 

create table Productos(
    CodProducto int PRIMARY key,
    Nombre VARCHAR(50) not null,
    Precio money not null
)
create table Clientes(
    Id int PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    edad int
)

create table Ventas (
    Id int PRIMARY key,
    IdProducto int not null,
    IdCliente int not null,
    Fecha DATE
)

alter TABLE Ventas add CONSTRAINT producto_ventas FOREIGN key (IdProducto) REFERENCES Productos(CodProducto)
alter TABLE Ventas add CONSTRAINT clientes_venstas FOREIGN key (IdCliente) REFERENCES Clientes(Id)

--4.
drop table Ventas;
drop table Productos;
drop table Clientes;

--5. 
sp_rename 'Clientes.Apellido', 'Apellidos', 'COLUMN';
--6.
delete FROM Ventas;
--or
TRUNCATE TABLE Ventas;
go

--7. 
--insertando productos
insert into Productos(CodProducto, Nombre, Precio) VALUES (1, 'Audifóno', 33.9);
insert into Productos(CodProducto, Nombre, Precio) VALUES (2, 'Alicate', 50);
insert into Productos(CodProducto, Nombre, Precio) VALUES (3, 'Martillo', 59);
insert into Productos(CodProducto, Nombre, Precio) VALUES (4, 'Combo', 120);
insert into Productos(CodProducto, Nombre, Precio) VALUES (5, 'Machete', 100);
--insertando clientes
insert into Clientes(Id, nombre, Apellidos, Edad) VALUES (100, 'Jose Luis', 'Silva Santos', 20);
insert into Clientes(Id, nombre, Apellidos, Edad) VALUES (200, 'Elvis', 'Lopez Gomez', 50);
insert into Clientes(Id, nombre, Apellidos, Edad) VALUES (300, 'Matias José', 'Rocha Santos', 30);
insert into Clientes(Id, nombre, Apellidos, Edad) VALUES (400, 'Maria Aparecida', 'Do Santos Neves', 33);
insert into Clientes(Id, nombre, Apellidos, Edad) VALUES (500, 'Adenicis', 'Jesus Oliveira', 28);

--insertando Ventas
insert into Ventas(Id, IdProducto, IdCliente, Fecha) VALUES (1000, 2, 100, '2018-08-20');
insert into Ventas(Id, IdProducto, IdCliente, Fecha) VALUES (2000, 4, 200, '2018-08-21');
insert into Ventas(Id, IdProducto, IdCliente, Fecha) VALUES (3000, 1, 500, '2018-05-22');
insert into Ventas(Id, IdProducto, IdCliente, Fecha) VALUES (4000, 5, 400, '2018-07-04');
insert into Ventas(Id, IdProducto, IdCliente, Fecha) VALUES (5000, 3, 300, '2018-08-02');

--8.
select *
from Ventas;

select v.Id, p.Nombre 'Producto', c.Nombre 'Cliente', p.precio, v.fecha
from ((Ventas as v inner join Productos p on v.IdProducto = p.CodProducto) inner join clientes c on c.Id = v.IdCliente) 
--9
select top(10) *
from clientes
--10
select Id, Nombre, Apellidos, Edad
from clientes
order by Edad

--11
select Id, Nombre, Apellidos, Edad
from clientes
where Edad > 20

--12
delete from Clientes
WHERE edad >= 20

--this is the most important change!